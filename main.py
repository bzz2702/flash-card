import tkinter
import pandas
import random

BACKGROUND_COLOR = "#B1DDC6"
USED_WORDS_INDEXES = []

# choose random French word

def random_french_word():
    df = pandas.read_csv("data/french_words.csv")
    list_of_dict_words = df.to_dict(orient="records")
    while True:
        random_num = random.randint(0, 100)
        if random_num in USED_WORDS_INDEXES:
            continue
        else:
            USED_WORDS_INDEXES.append(random_num)
            random_fr_word = list_of_dict_words[random_num]["French"]
            title_label.configure(text=f"{random_fr_word}")
            break
    print(USED_WORDS_INDEXES)


# setting up window
window = tkinter.Tk()
window.title("Flashy")
window.configure(background=BACKGROUND_COLOR, pady=20)

# setting up canvas
canvas = tkinter.Canvas(width=850, height=550)
canvas.configure(background=BACKGROUND_COLOR,  highlightthickness=0)

# setting up front card (starting) image
logo_img = tkinter.PhotoImage(file="images/card_front.png")
canvas.create_image(435, 280, image=logo_img)
canvas.grid(row=0, column=0, columnspan=2, rowspan=4)

# setting up buttons
wrong_button_image = tkinter.PhotoImage(file="images/wrong.png")
wrong_button = tkinter.Button(image=wrong_button_image, highlightthickness=0, command=random_french_word)
wrong_button.grid(row=4, column=0)

right_button_image = tkinter.PhotoImage(file="images/right.png")
right_button = tkinter.Button(image=right_button_image, highlightthickness=0, command=random_french_word)
right_button.grid(row=4, column=1)

# setting up word labels
title_label = tkinter.Label(text="Title", font=('Arial', 40, "italic"), bg="white")
title_label.grid(row=1, column=0, columnspan=2)

word_label = tkinter.Label(text="Word", font=('Arial', 60, "bold"), bg="white")
word_label.grid(row=2, column=0, columnspan=2)







window.mainloop()